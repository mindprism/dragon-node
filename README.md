# Dragon Node

Dragon Naturally Speaking command launcher via TSR NodeJs with help from AutoIt3

Usage `node index.js` then write commands to `data/dragon.ini` section `_` keys `app`, `p1`, `d`

- `app` : determines the *-dragon.ini to use for command lookups
- `p1` : the action command (series of words)
- `d` : some type of timestamp to ensure duplicate commands are seen as unique

## Meta Commands

In dragon.ini section __METAS__ are a series of keys with no values which represent Meta commands (commands available outside of an applications <app>-dragon.ini file)

Meta commands may take a wildcard at the end, or in the middle.

Example of end:

word1 word2|*

Example of middle:

word1 word2|word3

A wildcards value is typically a number, or a series of words.

## Conventions

From this point on "command" will have two meanings, the conventional and as a descriptor for Dragons COMMAND which often look like:

<list> word

Where list is a list of words or phrases that are in that location

move window <udlr>

for instance is a COMMAND which can contain many ORDERS:

move window up
move window down
move window left
move window right

These orders exist as sections in the applications <app>-dragon.ini file

An ORDER is composed of PARTICLES (the words themselves)

## Recall mechanism

Part of the functionality of Dragon Node is to provide a way for the user to recall what ORDERS he might say to a particular application.

This is done by using Meta Commands and CATEGORIES, the user says "show categories" and a categories list is shown. Each COMMAND resides in a CATEGORY.

So, the user can issue the Meta Command "show helper category" and see all the COMMANDS in the helper (or whatever) category.

By looking at a COMMAND list for the CATEGORY the user can compute all the ORDERS in that category, since a COMMAND represents many possible ORDERS.

## The Ini Files

Every app is given its own ini file, named <app>-dragon.ini

There is also a global ini named GLOBAL-dragon.ini

Every ORDER is an ini SECTION, written as

[move window right]

-ie-

[<ORDER>]

CATEGORIES in the ini are written as

[_HELPERS_]

-ie-

[_<CATEGORY>_]

All ini ORDER sections must reside below its CATEGORY section.

[_CATEGORY1_]

[order 1 belonging to category one]
[order 2 belonging to category one]

[_CATEGORY2_]

[order 1 belonging to category two]
[order 2 belonging to category two]

There are also two Meta sections, the __CATS__, and the __LISTS__ sections

Both of these have to be manually maintained and computed if new ORDERS are added.

The __CATS__ Meta section contains a key for every CATEGORY whose value is an ordered list of COMMANDS seperated by a vertical bar.

The __LISTS__ Meta section contains a key for every Dragon list used by the COMMANDS for this app. List values are also separated by a vertical bar.

List names starting with 0 are always manually computed, other lists have a form where the values can be computed from the name of the list, example:

fullscreen-fullmonitor=fullscreen|fullmonitor


### To Be Continued...



