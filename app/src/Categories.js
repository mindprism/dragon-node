/**
 * Created by Administrator on 11/21/2016.
 */
class Categories {
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.realList); // !unuf - lint hack for webstorm
  }

  constructor(_) {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    this.__ = _;
  }

  get _() {
    return this.__;
  }

  get ini() {
    return this._.inis.file('ini');
  }

  _lineIsCategory(l) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    return /\[_[a-zA-Z0-9-]+_]/.test(l);
  }

  _nameFromCategoryLine(l) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    return l.substr(2, l.length - 4);
  }

  realList() {
    let a = this.ini.fileArray();
    let b = [];
    for (let x = 0, x_len = a.length; x < x_len; x = x + 1) {
      let i = a[x];
      if (this._lineIsCategory(i)) {
        b.push(this._nameFromCategoryLine(i));
      }
    }
    return b;
  }
}
module.exports = Categories;
