/**
 * Created by Administrator on 11/21/2016.
 */
const path = require('path');
const fs = require('fs');
const get = require('./object-path').get;
//const set = require('./object-path').set;
const process = require('child_process');
class DotExe {
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.draw); // !unuf - lint hack for webstorm
  }

  constructor(_) {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    this.__ = _;
  }

  get _() {
    return this.__;
  }

  get cfg() {
    return this._.inis.file('cfg');
  }

  get dotExe() {
    return get(this.cfg.data, '_.dot');
  }

  get dotOutDir() {
    return get(this.cfg.data, '_.dot_out_dir');
  }

  get dotName() {
    return path.join(this.dotOutDir, this.dotNameName());
  }

  get dotNameName() {
    return get(this._.data, 'p1').replace(' ', '-');
  }

  draw() {
    let fin = this.dotName() + '.dot';
    let file_out = this.dotNameName() + '.jpg';
    let out_flag = '-o' + file_out;
    let type_flag = '-T'+'jpg';
    let flags = type_flag + ' ' + out_flag;
    let cmd = '"' + this.dotExe + '"' + ' ' + flags + ' ' + '"' + fin + '"';
    let wd = path.dirname(fs.realpathSync(__filename));
    wd = path.join(wd, 'data', 'dot');
    //noinspection JSUnresolvedFunction
    process.execSynch(cmd, {cwd: wd});
    //RunWait($cmd,$dir,@SW_HIDE)
  }
}
module.exports = DotExe;
