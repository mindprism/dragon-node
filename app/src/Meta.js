/**
 * Created by Administrator on 11/20/2016.
 */
const get = require('./object-path').get;
//const set = require('./object-path').set;

class Meta{
  webstorm(){
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.check); // !unuf - lint hack for webstorm
  }
  constructor(_){
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    this.__=_;
  }
  get _(){
    return this.__;
  }
  get callables(){
    return this._.callables;
  }
  get log(){
    return this._.utils.log;
  }
  get metas(){
    return get(this._.inis.file('dragon').data, '__METAS__');
  }
  get thisApp(){
    return get(this._.inis.file('dragon').data, '_.app', '');
    //return this._.data.thisApp;
  }
  set thisApp(v){
    this.log('set thisApp', 'thisApp', this._.data.thisApp);
    this.log('set thisApp', '----app', v);
    this._.data.thisApp=v;
    this.inis.ensure();
  }
  get inis(){
    return this._.inis;
  }
  get p1(){
    return this._.data.p1;
  }
  set p1(v){
    this._.data.p1=v;
  }
  set key0(v){
    this._.data.key0=v;
  }
  // Z_DOT cmd check():void meta._ ::index.js z.65464181005652301.2016.11.21.05.02.26.456|+!
    /**
     * check():void - main entry
     * @return {void}
     */
  check() {
    const p1 = this.p1;
    let p1_ = p1;
    let had_this = false;
    if ('this ' === p1_.substr(0, 5)) {
      had_this = true;
      p1_ = p1.substr(6);
    }
    this.log('check', 'had_this', had_this);
    const is_meta = this._isMeta(p1_);
    this.log('check', 'is_meta', is_meta);
    if (is_meta) {
      if (had_this) {
        this._refreshThisApp();
        this.p1 = p1_;
        if (!this._tryMeta()) {
          console.log('ERROR', 'did not find in _TryMeta A');
        }
      } else {
        this.thisApp='GLOBAL';
        if (!this._tryMeta()) {
          console.log('ERROR', 'did not find in _TryMeta B');
        }
      }
    } else {
      this._refreshThisApp();
      let did = this._.main.process();
      if (!did) {
        console.log('ERROR', 'did not find in _TryMeta C');
      }
    }
  }//-check
  // Z_DOT cmd _refreshThisApp():void ::Meta.js z.61372242005652301.2016.11.21.06.43.47.316|-!
  /**
   * _refreshThisApp() - reload string from dragon.ini
   * @return {void} nada
   */
  _refreshThisApp(){
    this.thisApp=this.thisApp;
  }//-_refreshThisApp
  // Z_DOT fn _isMeta(p1_):boolean meta._ ::index.js z.71127322005652301.2016.11.21.06.12.52.117|-?
  /**
   * _isMeta(p1_):boolean
   * @param {string} p1_ - p1 without 'this'
   * @return {boolean} true if meta command
   */
  _isMeta(p1_) {
    const metas = this.metas;// cache
    let rv = this._isMetaFirstNoLast(this._firstNoLast(metas), p1_);
    if (!rv) {
      rv = this._isMetaFirstStarry(this._firstStarry(metas), p1_);
    }
    if (!rv) {
      rv = this._isMetaFirstLast(this._firstLast(metas), p1_);
    }
    return rv;
  }
  
  _isMetaFirstLast(arr, p1_) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv = false;
    for (let x = 0, len = arr.length; x < len; x = x + 1) {
      let v = arr[x];
      let first = v.first;
      let last = v.last;
      // is left of main parm eq to item first and space
      // and is right of main parm eq to last and space
      if (p1_.substr(0, first.length + 1) === first + ' '
        && p1_.substr(p1_.length - (last.length + 1)) === ' ' + last) {
        rv = true;
        break;
      }
    }
    return rv;
  }
  
  _isMetaFirstStarry(arr, p1_) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv = false;
    for (let x = 0, len = arr.length; x < len; x = x + 1) {
      let v = arr[x];
      // is left of main parm eq to item and space
      if (p1_.substr(0, v.length + 1) === v + ' ') {
        rv = true;
        break;
      }
    }
    return rv;
  }
  
  _isMetaFirstNoLast(arr, p1_) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv = false;
    for (let x = 0, len = arr.length; x < len; x = x + 1) {
      let v = arr[x];
      // is main parm eq to item
      if (p1_ === v) {
        rv = true;
        break;
      }
    }
    return rv;
  }

//-_isMeta

  _firstNoLast(metas) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    const o = metas;
    const a = [];
    for (let key in o) {
      if (o.hasOwnProperty(key)) {
        if ('' === o[key]) {
          a.push(key);
        }
      }
    }
    return a;
  }
  _firstStarry(metas) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    const o = metas;
    const a = [];
    for (let key in o) {
      if (o.hasOwnProperty(key)) {
        if ('*' === o[key]) {
          a.push(key);
        }
      }
    }
    return a;
  }
  _firstLast(metas) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    const o = metas;
    const a = [];
    for (let key in o) {
      if (o.hasOwnProperty(key)) {
        if ('' !== o[key] && '*' !== o[key]) {
          a.push({first: key, last: o[key]});
        }
      }
    }
    return a;
  }
  _tryMeta() {
    const o = this.metas;
    let did = false;
    for (let key in o) {
      if (o.hasOwnProperty(key)) {
        did = this._callCheck(key, o[key]);
        if (did) {
          break;
        }
        //console.log(key + " -> " + p[key]);
      }
    }
    return did;
  }//-_tryMeta
  _callCheck(first, last) {
    this.log('_callCheck', 'first', first);
    this.log('_callCheck', 'first', first, 1);
    this.log('_callCheck', 'last', last);
    let did = false;
    const p1=this.p1;
    if ('' === last) {
      if (p1.substr(0, first.length) === first) {
        this.log('_callCheck', 'branch', 'last-nada');
        did = this._callIt(first, last);
      }
    }
    if ('*' === last) {
      if (p1.substr(0, first.length + 1) === first + ' ') {
        this.log('_callCheck', 'branch', 'last-star');
        did = this._callIt(first, last);
      }
    }
    if (p1.substr(0, first.length + 1) === first + ' '
        && p1.substr(p1.length - (last.length + 1)) === ' ' + last) {
      this.log('_callCheck', 'branch', 'last-val');
      did = this._callIt(first, last);
    }
    return did;
  }//-_callCheck
  _callIt(first, last) {
    ///  Verb Adj Noun
    ///  Copy All Commands
    ///  Noun Adj Verb
    ///  Commands All Copy
    const first_a = first.split(' ');
    //
    const flip=function() {
      const first_ = first_a[0];
      const last_ = first_a[first_a.length - 1];
      const a = first_a.slice();
      a[0] = last_;
      a[a.length - 1] = first_;
      return a.join('_');
    };
    //
    let fn;
    let did = false;
    this.log('_callIt', 'first', first);
    this.log('_callIt', 'last', last);
    const try_call= () => {
      let rv = false;
      if ('function' === typeof this.callables[fn]) {
        this.callables[fn]();
        did = true;
        rv = true;
      } else {
        this.log('_callIt', 'cannot find', fn);
        rv = false;
      }
      return rv;
    };
    //
    if ('' === last) {
      fn = first_a.join('_');
      if (!try_call()) {
        fn = flip();
        try_call();
      }
    }//- lest===''
    if ('*' === last) {
      this.key0 = this.p1.substr(first.length + 2);
      fn = first_a.join('_') + '_';
      if (!try_call()) {
        fn = flip() + '_';
        try_call();
      }
    } else {//-last==='*'
      /// noun adjs X verb
      let a = [];
      a.push(last);//noun
      a = a.concat(first_a.slice(1));
      a.push('_');
      a.push(a[0]);
      fn = a.join('_');
      try_call();
    }
    return did;
  }//-_callIt
}

module.exports = Meta;
