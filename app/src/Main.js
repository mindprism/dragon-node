/**
 * Created by Administrator on 11/21/2016.
 */
const path = require('path');
const get = require('./object-path').get;
//
class Main {
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.process); // !unuf - lint hack for webstorm
  }

  constructor(_) {
    this.__ = _;
  }

  get _() {
    return this.__;
  }

  get log() {
    return this._.utils.log;
  }

  get p1() {
    return this._.data.p1;
  }

  get thisApp() {
    return this._.data.thisApp;
  }

  get autoIt() {
    return this._.autoIt;
  }

  get scriptDir() {
    return this._.utils.scriptDir();
  }

  get current(){
    return this.inis.current();
  }

  //noinspection FunctionWithMoreThanThreeNegationsJS
  process() {
    const me = 'Main.process';
    this.log(me, 'p1', this.p1, 1);
    this.log(me, 'thisApp', this.thisApp, 1);
    let did = this.process_k();
    if (!did) {
      did = this.process_m();
    }
    if (!did) {
      did = this.process_s();
    }
    if (!did) {
      did = this.process_e();
    }
    if (!did) {
      if ('GLOBAL' !== this.thisApp) {
        this._.data.thisApp = 'GLOBAL';
        did = this.process();
      }
    }
    return did;
  }

  process_e() {
    const me = 'Main.process_e';
    let e = this.current.currentKeyValue('e', '');
    let rv = false;
    if ('' !== e) {
      let n = '"' + e + '"';
      let cmd = 'vs.exe ' + n;
      this._run(cmd);
      this.log(me, 'cmd', cmd);
      rv = true;
    }
    return rv;
  }

  process_s() {
    const me = 'Main.process_s';
    let s = this.current.currentKeyValue('s', '');
    let rv = false;
    if ('' !== s) {
      let sd = this.scriptDir;
      sd = path.join(sd, 'scripts');
      let a = s.split(' ');
      let n = a[0];
      let r = '';
      if (1 < a.length) {
        r = s.substr(n.length + 1);
      }
      let fn = path.join(sd, n) + '.au3';
      let cfg_data = this._.inis.file('cfg').data;
      let exe = get(cfg_data, '_.autoit_exe');
      let cmd = '"' + exe + '" "' + fn + '" ' + r;
      this._run(cmd, sd);
      this.log(me, 'cmd', cmd);
      rv = true;
    }
    return rv;
  }

  process_m() {
    const me = 'Main.process_m';
    let m = this.current.currentKeyValue('m', '');
    let rv = false;
    if ('' !== m) {
      let cmd = '"-#' + m + '"';
      this._toolTip(cmd);
      this._run('vs.exe ' + cmd);
      this.log(me, 'cmd', cmd);
      rv = true;
    }
    return rv;
  }

  process_k() {
    let k = this.current.currentKeyValue('k', '');
    let rv = false;
    if ('' === k) {
      rv = false;
    } else {
      const me = 'Main.process_k';
      let kp = this.current.currentKeyValue('kp', '0');
      kp = parseInt(kp, 10);
      this._keyHook(k, kp);
      this.log(me, 'k', k);
      this.log(me, 'kp', kp);
      this._toolTip(k, 0, 0);
      rv = true;
    }
    return rv;
  }

  _keyHook(k, kp) {
    let lk = k.toLowerCase();
    if ('WEBSTORM' === this.thisApp && '{f12}' === lk.substr(0, 5)) {
      let nk = k.substr(6);
      let mid = nk.substr(6, lk.indexOf('{enter}') - 6);
      let rest = k.substr(mid.length + 6);
      this._sendWebstorm(mid, rest);
    } else {
      this._send(k, kp);
    }
  }

  _run(cmd, dir = null, flag = null) {
    this.autoIt.run(cmd, dir||this.scriptDir, flag);
  }

  _toolTip(s, x = 0, y = 0) {
    this.autoIt.toolTip(s, x, y);
  }

  _send(k, kp) {
    this.autoIt.send(k, kp);
  }

  _sleep(ms) {
    this.autoIt.sleep(ms);
  }

  _sendWebstorm(mid, rest) {
    //this.autoIt.Send(k,kp);
    this._send('{f12}');
    //noinspection MagicNumberJS
    this._sleep(300);
    this._send(mid, 1);
    //noinspection MagicNumberJS
    this._sleep(500);
    this._send(rest);
  }
}
module.exports = Main;
