//const chokidar = require('chokidar');
//import chokidar from 'chokidar';
//const fs = require('fs');
//const ini = require('ini');
//;const objectPath = require('object-path');
const fs = require('fs');
const moment = require('moment');
moment.ts = function() {
  return moment().format('YYYY-MM-DD-HH-mm-ss-SSS');
};
const path = require('path');
const get = require('./object-path').get;
//const set = require('./object-path').set;
const consts = require('./Consts');
const Inis = require('./Inis');
const Meta = require('./Meta');
const Main = require('./Main');
const AutoIt = require('./AutoIt');
//const IniFile = require('./IniFile');
const Log = console.log.bind(console); // eslint-disable-line no-unused-vars
//const DRAGON_INI = './data/dragon.ini';
//e C:\!mark\_tool\dragonnode_vpw\app\data\dragon.ini
//const DRAGON_CFG_INI = './data/dragon-cfg.ini';
//e C:\!mark\_tool\dragonnode_vpw\app\data\dragon-cfg.ini
//e C:\!mark\_tool\dragonnode_vpw\app\data\dragon-log.ini
//
//
/**
 * Main App
 */
const _ = {
  _name: '_'
  // Z_DOT mod data:object _ ::index.js z.79084371005652301.2016.11.21.04.49.08.097|+,
    /**
     * _.data - Data store, globals
     */
  , data: {
    _name: 'data'
    // Z_DOT var thisApp:string data._ ::index.js z.06972422005652301.2016.11.21.06.13.47.960|+,
    /**
     * _.data.thisApp - used to select *.ini
     */
    , thisApp: 'GLOBAL'
  }
  , consts: consts
  // Z_DOT mod utils:object _ ::index.js z.18135471005652301.2016.11.21.04.50.53.181|+,
    /**
     * _.utils - Utility module
     */
  , utils: {
    _name: 'utils'
    // Z_DOT cmd log(sec, key, val, ts):void utils._ ::index.js z.22383877004652301.2016.11.20.21.37.18.322|+!
      /**
       * _.utils.log(sec, key, val, ts):void - Log to dragon-log.ini
       * @param {string} sec - section of ini
       * @param {string} key - key of section of ini
       * @param {string} val - value of key
       * @param {boolean} ts - do timestamp on key
       * @return {void}
       */
    , log: function(sec, key, val, ts) {
      const o = {};
      o[sec] = {};
      const tsa = ts ? '-' + moment.ts() : '';
      const kk = key + tsa;
      o[sec][kk] = val;
      _.inis.file('logger').set(o);
    }
    , scriptDir: function(){
      if ('undefined' === typeof _.utils.scriptDir.value) {
        _.utils.scriptDir.value=path.dirname(fs.realpathSync(__filename));
      }
      return _.utils.scriptDir.value;
    }
  }
  // Z_DOT mod dragon:object _ ::index.js z.62138671005652301.2016.11.21.04.54.43.126|+,
    /**
     * _.dragon - Dragon module, dragon.ini
     */
  , dragon: {
    _name: 'dragon'
    , webstorm: function() {
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.onChange); // !unuf - lint hack for webstorm
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    }
    // Z_DOT evt onChange(event, stat):void dragon._ ::index.js z.76006771005652301.2016.11.21.04.56.00.067|%\
      /**
       * _.dragon.onChange(event, stat):void - dragon.ini change event handler
       * @param {string} event - 'change'
       * @param {object} stat - status object
       * @returns {void} nada
       */
    , onChange: function(event, stat) {
      this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](event, stat); // !unup - lint hack for webstorm
      const d = _.inis.file('dragon');
      const data = d.data;
      if (data._.d!==_.data.d) {
        _.data.p1 = get(d.data,'_.p1');
        _.data.thisApp = get(d.data,'_.app');
        _.data.d = d.data._.p1;
        Log('dragon on change, p1:',_.data.p1);
        Log('dragon on change, thisApp:',_.data.thisApp);
        Log('dragon on change, d:',_.data.d);
        _.meta.check();
      }
    }
  }
  // Z_DOT mod meta:object _ ::index.js z.73662081005652301.2016.11.21.05.00.26.637|+,
    /**
     * _.meta - Meta Command module
     */
  , meta: {
    _name: 'meta'
  }
  , inis: {
    _name: 'inis'
  }
  , main: {
    _name: 'main'
  }
  , inits: {
    _name: 'inits'
    , init: function() {
      _.inis = new Inis(_);
      _.main = new Main(_);
      _.meta = new Meta(_);
      _.autoIt = new AutoIt(_);
    }
  }
  , callables: {
    _name: 'callables'
    ,meta_info: function(){
      console.log('CALLED','meta_info');
    }
  }
  , init: function() {
    this.inits.init();
  }
};
_.init();
global._=_;
//_.utils.log('_', 'test', 'ok');
//_.utils.log('_', 'test', 'ok', 1);
Log('ready');
Log(_.inis.file('dragon').data);
