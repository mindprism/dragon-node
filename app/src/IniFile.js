const fs = require('fs');
const ini = require('ini');
const chokidar = require('chokidar');
const get = require('./object-path').get;
// Z_DOT IniFile:class ::IniFile.js z.99630377004652301.2016.11.20.21.28.23.699|class
class IniFile {
  /**
   * webstorm linter hack
   * @return {void}
   */
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.file); // !unuf - lint hack for webstorm
    this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](IniFile); // !unuc - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.set); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.dispose); // !unuf - lint hack for webstorm
  }

  /**
   * IniFile ctor
   * @param {object} _ app
   * @param {string} file     ini file path
   * @param {function} onChange function callback
   * @return {void}
   */
  constructor(_, file, onChange = null) {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    this.__=_;
    this._file = file;
    this.onChange=onChange;
    //if (onChange) {this._onChange = onChange;}
    if (!fs.existsSync(this._file)) {
      fs.writeFileSync(this._file, '');
    }
    this._watcher = chokidar.watch(this._file, {ignored: /[\/\\]\./, persistent: true});
    this._watcher.on('change', this._onChange);
  }

  dispose(){
    if (this._watcher) {
      this._watcher.close();
    }
  }

  /**
   * dummy event placeholder signature
   * http://nodejs.org/api/fs.html#fs_class_fs_stats
   * @param {string} event      'change'
   * @param {object} stats      obj, see url
   * @return {void}
   */
  _onChange(event, stats) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](event, stats); // !unup - lint hack for webstorm
    if (this.onChange) {
      this.onChange(event, stats);
    }
    this._getData();
  }

  /**
   * Set, merge and write
   * @param {object} o    object to merge
   * @return {void}
   */
  set(o) {
    //noinspection JSCheckFunctionSignatures
    this.data = Object.assign({}, this.data, o);
  }

  currentKeyValue(key, def){
    let o=get(this.data,this.p1,{});
    return get(o,key,def);
  }

  /**
   * Subject ini file, read only
   */
  get file() {
    return this._file;
  }

  get fileArray(){
    let a=[];
    if (fs.existsSync(this.file)) {
      a=fs.readFileSync(this.file).toString().replace("\r",'').split("\n");
    }
    return a;
  }

  _getData(){
    this._data=ini.parse(fs.readFileSync(this._file, 'utf-8'));
  }

  get _(){
    return this.__;
  }
  
  get p1(){
    return this._.data.p1;
  }
  
  /**
   * Ini file as an object, must set this whole to update
   */
  get data() {
    //return ini.parse(fs.readFileSync(this._file, 'utf-8'));
    return this._data;
  }

  set data(d) {
    fs.writeFileSync(this._file, ini.stringify(d));
  }
}
module.exports = IniFile;
