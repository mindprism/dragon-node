/**
 * Created by Administrator on 11/21/2016.
 * https://github.com/xhawk18/node-autoit
 */
const au = require('autoit');
class AutoIt {
  //noinspection FunctionTooLongJS
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.setOption); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.clipGet); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.clipPut); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseClick); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseClickDrag); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseDown); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseGetCursor); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseGetPos); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseMove); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseUp); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.mouseWheel); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.opt); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.pixelGetColor); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.pixelSearch); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.processClose); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.processExists); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.runWait); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.send); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.sleep); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.toolTip); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winActivate); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winActive); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winClose); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winExists); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetCaretPos); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetClassList); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetClientSize); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetHandle); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetHandleAsText); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetPos); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetProcess); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetText); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winGetTitle); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winKill); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winMenuSelectItem); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winMinimizeAll); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winMinimizeAllUndo); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winMove); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winSetOnTop); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winSetState); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winSetTitle); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winSetTrans); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winWait); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winWaitActive); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winWaitClose); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.winWaitNotActive); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.sendMessage); // !unuf - lint hack for webstorm
  }

  constructor(_) {
    this.__ = _;
    //noinspection JSUnresolvedFunction
    au.Init();
  }

  setOption(option, value) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.AutoItSetOption(option, value);
  }

  clipGet(size = 0) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (0 === size) {
      //noinspection JSUnresolvedFunction
      rv = au.ClipGet();
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.ClipGet(size);
    }
    return rv;
  }

  clipPut(str) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.ClipPut(str);
  }

  mouseClick(button, x = null, y = null, clicks = null, speed = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === x) {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClick(button);
    } else if (null === clicks) {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClick(button, x, y);
    } else if (null === speed) {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClick(button, x, y, clicks);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClick(button, x, y, clicks, speed);
    }
    return rv;
  }

  mouseClickDrag(button, xy, xy2, speed = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === speed) {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClickDrag(button, xy.x, xy.y, xy2.x, xy2.y);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.MouseClickDrag(button, xy.x, xy.y, xy2.x, xy2.y, speed);
    }
    return rv;
  }

  mouseDown(button) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.MouseDown(button);
  }

  mouseGetCursor() {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.MouseGetCursor();
  }

  mouseGetPos() {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.MouseGetPos();
  }

  mouseMove(x, y, speed = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === speed) {
      //noinspection JSUnresolvedFunction
      rv = au.MouseMove(x, y);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.MouseMove(x, y, speed);
    }
    return rv;
  }

  mouseUp(button) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.MouseUp(button);
  }

  mouseWheel(direction, clicks) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.MouseWheel(direction, clicks);
  }

  opt(option, val) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.Opt(option, val);
  }

  pixelGetColor(x, y) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.PixelGetColor(x, y);
  }

  pixelSearch(rect, col, variance = null, step = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === variance) {
      //noinspection JSUnresolvedFunction
      rv = au.PixelSearch(rect, col);
    } else if (null === step) {
      //noinspection JSUnresolvedFunction
      rv = au.PixelSearch(rect, col, variance);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.PixelSearch(rect, col, variance, step);
    }
    return rv;
  }

  processClose(process) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.ProcessClose(process);
  }

  processExists(process) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.ProcessExists(process);
  }

  run(cmd,dir=null,flag=null){
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === dir) {
      //noinspection JSUnresolvedFunction
      rv = au.Run(cmd);
    }else if (null === flag) {
      //noinspection JSUnresolvedFunction
      rv = au.Run(cmd,dir);
    }else{
      //noinspection JSUnresolvedFunction
      rv = au.Run(cmd,dir,flag);
    }
    return rv;
  }

  runWait(cmd,dir=null,flag=null){
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === dir) {
      //noinspection JSUnresolvedFunction
      rv = au.RunWait(cmd);
    }else if (null === flag) {
      //noinspection JSUnresolvedFunction
      rv = au.RunWait(cmd,dir);
    }else{
      //noinspection JSUnresolvedFunction
      rv = au.RunWait(cmd,dir,flag);
    }
    return rv;
  }

  send(keys, mode = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === mode) {
      //noinspection JSUnresolvedFunction
      rv = au.Send(keys);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.Send(keys, mode);
    }
    return rv;
  }

  sleep(ms) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.Sleep(ms);
  }

  toolTip(text, x = null, y = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    if (null === x) {
      //noinspection JSUnresolvedFunction
      au.ToolTip(text);
    } else {
      //noinspection JSUnresolvedFunction
      au.ToolTip(text, x, y);
    }
  }

  winActivate(title, text = '') {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if ('' === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinActivate(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinActivate(title, text);
    }
    return rv;
  }

  winActive(title, text = '') {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if ('' === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinActive(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinActive(title, text);
    }
    return rv;
  }

  winClose(title, text = '') {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if ('' === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinClose(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinClose(title, text);
    }
    return rv;
  }

  winExists(title, text) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if ('' === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinExists(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinExists(title, text);
    }
    return rv;
  }

  winGetCaretPos() {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinGetCaretPos();
  }

  winGetClassList(title, text = null, size = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetClassList(title);
    } else if (null === size) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetClassList(title, text);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetClassList(title, text, size);
    }
    return rv;
  }

  winGetClientSize(title, text = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetClientSize(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetClientSize(title, text);
    }
    return rv;
  }

  winGetHandle(title, text = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetHandle(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetHandle(title, text);
    }
    return rv;
  }

  winGetHandleAsText(title, text = null, size = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetHandleAsText(title);
    } else if (null === size) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetHandleAsText(title, text);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetHandleAsText(title, text, size);
    }
    return rv;
  }

  winGetPos(hwnd) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinGetPos(hwnd);
  }

  winGetProcess(title, text = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetProcess(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetProcess(title, text);
    }
    return rv;
  }

  winGetText(title, text = null, size = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetText(title);
    } else if (null === size) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetText(title, text);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetText(title, text, size);
    }
    return rv;
  }

  winGetTitle(title, text = null, size = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetTitle(title);
    } else if (null === size) {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetTitle(title, text);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinGetTitle(title, text, size);
    }
    return rv;
  }

  winKill(title, text = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === text) {
      //noinspection JSUnresolvedFunction
      rv = au.WinKill(title);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.WinKill(title, text);
    }
    return rv;
  }

  winMenuSelectItem(title, text = '', items) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (5 === items.length) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMenuSelectItem(title, text, items[0], items[1], items[2], items[3], items[4]);
    } else if (4 === items.length) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMenuSelectItem(title, text, items[0], items[1], items[2], items[3]);
    } else if (3 === items.length) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMenuSelectItem(title, text, items[0], items[1], items[2]);
    } else if (2 === items.length) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMenuSelectItem(title, text, items[0], items[1]);
    } else if (1 === items.length) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMenuSelectItem(title, text, items[0]);
    }
    return rv;
  }

  winMinimizeAll() {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.WinMinimizeAll();
  }

  winMinimizeAllUndo() {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    au.WinMinimizeAllUndo();
  }

  winMove(title, text, xy, wh = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === wh) {
      //noinspection JSUnresolvedFunction
      rv = au.WinMove(title, text, xy.x, xy.y);
    } else {
      //noinspection JSUnresolvedFunction,JSUnresolvedVariable
      rv = au.WinMove(title, text, xy.x, xy.y, wh.w, wh.h);
    }
    return rv;
  }

  winSetOnTop(title, text, flag) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinSetOnTop(title, text, flag);
  }

  winSetState(title, text, flags) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinSetState(title, text, flags);
  }

  winSetTitle(title, text, newTitle) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinSetTitle(title, text, newTitle);
  }

  winSetTrans(title, text, val) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinSetTrans(title, text, val);
  }

  winWait(title, text, time) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinWait(title, text, time);
  }

  winWaitActive(title, text, time) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinWaitActive(title, text, time);
  }

  winWaitClose(title, text, time) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinWaitClose(title, text, time);
  }

  winWaitNotActive(title, text, time) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    //noinspection JSUnresolvedFunction
    return au.WinWaitNotActive(title, text, time);
  }

  sendMessage(hwnd, msg, wparm = null, lparm = null) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv;
    if (null === wparm) {
      //noinspection JSUnresolvedFunction
      rv = au.SendMsg(hwnd, msg);
    } else if (null === lparm) {
      //noinspection JSUnresolvedFunction
      rv = au.SendMsg(hwnd, msg, wparm);
    } else {
      //noinspection JSUnresolvedFunction
      rv = au.SendMsg(hwnd, msg, wparm, lparm);
    }
    return rv;
  }
}
module.exports = AutoIt;
