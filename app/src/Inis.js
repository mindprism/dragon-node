/**
 * Created by Administrator on 11/20/2016.
 */

const IniFile = require('./IniFile');
const get = require('./object-path').get;
//const set = require('./object-path').set;
class Inis {
  webstorm() {
  }

  constructor(_) {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    this.__ = _;
    //this._dragon_ini = _.consts.DRAGON_INI;
    //this._dragon_cfg_ini = _.consts.DRAGON_CFG_INI;
    //
    this.files = {};
    this._addFile('cfg', _.consts.DRAGON_CFG_INI);
    this._addFile('dragon', _.consts.DRAGON_INI, _.dragon.onChange.bind(_.dragon));
    this._addFile('logger', get(this.file('cfg').data, '_.log', ''));
  }

  file(name){
    return this.files[name];
  }

  get _() {
    return this.__;
  }

  get log() {
    return this._.utils.log;
  }

  get thisApp() {
    return get(this.files.dragon.data, '_.app', '');
    //return this._.data.thisApp;
  }

  _addFile(name, path, callback = null) {
    if ('undefined' === typeof this.files[name]) {
      this.files[name] = new IniFile(path, callback);
    } else {
      console.log('Inis._addFile, name is already in use:' + name);
    }
  }

  current(){
    return this.files[this.thisApp];
  }

  ensure() {
    if ('undefined' === typeof this.files[this.thisApp]) {
      this._addFile(this.thisApp, './data/' + this.thisApp + '-dragon.ini');
    }
  }
}
module.exports = Inis;
