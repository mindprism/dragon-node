/**
 * Created by Administrator on 11/20/2016.
 */

class Consts{
  //noinspection FunctionNamingConventionJS
  /**
   * Relative path to dragon.ini
   * @return {string} the path
   */
  static get DRAGON_INI(){
    return './data/dragon.ini';
  }
  //noinspection FunctionNamingConventionJS
  /**
   * Relative path to dragon-cfg.ini
   * @return {string} the path
   */
  static get DRAGON_CFG_INI(){
    return './data/dragon-cfg.ini';
  }
}
module.exports = Consts;
