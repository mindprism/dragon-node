/**
 * Created by Administrator on 11/20/2016.
 */
const objectPath = require('object-path');

const EMPTY = {};
// Z_DOT cmd set(o, p, v):void ::object-path::app::dragonnode_vpw::_tool::!mark::C::index.js  z.82558567004652301.2016.11.20.21.16.25.528|+!
  /**
   * Set using objectPath
   *
   * @param {object} o      target to receive values
   * @param {string} p      path - dotted string
   * @param {*}      v      value to set
   * @param {boolean} no_replace - false
   * @return {*}     old value or object if path is missing
   *
   * @example var o;
   * set(o,'x.y','z');
   * // o={x:{y:'z'}
   */
    function set(o, p, v, no_replace=false) { // eslint-disable-line no-unused-vars
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](set); // !unuf - lint hack for webstorm
      //noinspection JSUnresolvedFunction
      return objectPath.set(o, p, v, no_replace);
    }
// Z_DOT fn get(o, p, v):value ::object-path::app::dragonnode_vpw::_tool::!mark::C::index.js  z.49714567004652301.2016.11.20.21.15.41.794|+?
  /**
   * Get using ObjectPath
   * Retrieve value from object using path string
   *
   * @param {object} o      target object which has values
   * @param {string} p      path string - dotted
   * @param {*}      v      default value
   * @return {*}     value
   *
   * @example var o={x:{y:'z'}};
   * log(get(o,'x.y')) // logs 'z'
   */
    function get(o, p, v = EMPTY) {
      let vv;
      if (v !== EMPTY) {
        vv = v;
      }
      //noinspection JSUnresolvedFunction
      return objectPath.get(o, p, vv);
    }

module.exports = {
  set
  ,get
};
