/**
 * Created by Administrator on 11/21/2016.
 */
class TableLabel {
  /** @namespace TableLabel.should */
  /**
   * Lint hacks
   * @return {void} nada
   */
  webstorm() {
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._optionAlt); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._esc); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._table); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._item); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._td); // !unuf - lint hack for webstorm
    this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this._font); // !unuf - lint hack for webstorm
  }

  constructor(_, nnc, options) {
    this.__ = _;
    this._nnc = nnc;
    /*
     nnc={
     num:{val,hdr}
     name:{val,hdr}
     count:{val,hdr}
     }
     */
    this._options = Object.assign({}, TableLabel.defaultOptions, options);
  }

  get nnc() {
    return this._nnc;
  }

  get options() {
    return this._options;
  }

  _esc(s) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    return s.replace('<', '&lt;').replace('>', '&gt;');
  }

  _optionAlt(name) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let rv = name.toUpperCase();
    if ('pointSize' === name) {
      rv = 'POINT-SIZE';
    }
    return rv;
  }

  _optionString(o, name) {
    let rv;
    let val = o[name];
    if ('undefined' === typeof val || null === val) {
      rv = '';
    } else if ('function' === typeof val) {
      //
      let nnc = this.nnc;
      //noinspection JSUnresolvedVariable
      val = val(this, nnc.num.val, nnc.count.val, nnc.name.val);
    }
    if ('undefined' === typeof rv) {
      rv = ' ' + this._optionAlt(name) + '="' + val + '"';
    }
    return rv;
  }

  _table(inner) {
    let a = [];
    let opt = this.options.table;
    a.push('<TABLE');
    for (let n in opt) {
      if (!opt.hasOwnProperty(n)) {
        continue;
      }
      let v = this._optionString(opt, n);
      if ('' !== v) {
        a.push(v);
      }
    }
    let topp = a.join('');
    topp = topp + '>';
    topp = topp + inner;
    topp = topp + '</TABLE>';
    return topp;
  }

  _isWhatProp(what, name) {
    this["CAN BE STATIC\n"] && this["CAN BE STATIC\n"](this); // !static - lint hack for webstorm
    let o = {
      td: {
        port: true
        , border: true
        , sides: true
        , cellSpacing: true
        , cellPadding: true
        //
        , colSpan: true
        , rowSpan: true
        //
        , height: true
        , width: true
        , fixedSize: true
        , align: true
        , valign: true
        , balign: true
        //
        , bgColor: true
        , color: true
      }
      , font: {
        face: true
        , pointSize: true
        , color: true
        , bold: true
        , italic: true
        , underline: true
        , strike: true
      }
    };
    return true === o[what][name];
  }

  _font(opt){
    let a = [];
    a.push('<TD');
    for (let n in opt) {
      if (!opt.hasOwnProperty(n)) {
        continue;
      }
      if (this._isWhatProp('font', n)) {
        let v = this._optionString(opt, n);
        if ('' !== v) {
          a.push(v);
        }
      }
    }
    
  }
  
  _item(who) {
    this["UNUSED PARAM\n"] && this["UNUSED PARAM\n"](who); // !unup - lint hack for webstorm
    // let opt_h = this.options[who].head;
    // let opt = this.options[who];
    // let a = [];
    // let nnc = this.nnc;
  }

  _td(who) {
    let opt = this.options[who];
    let a = [];
    a.push('<TD');
    for (let n in opt) {
      if (!opt.hasOwnProperty(n)) {
        continue;
      }
      if ('head' === n) {
        continue;
      }
      if (this._isWhatProp('td', n)) {
        let v = this._optionString(opt, n);
        if ('' !== v) {
          a.push(v);
        }
      }
    }
    let topp = a.join('');
    topp = topp + '>';
    // TODO more
    return topp;
  }
}
TableLabel.defaultOptions = {
  table: {
    border: 0
    , cellSpacing: 3
    , cellPadding: 4
    , bgColor: null
    , color: null
    , align: null
    , valign: null
    , port: null
  }
  , num: {
    port: 'num'
    , border: 1
    , sides: null
    , cellSpacing: null
    , cellPadding: null
    //
    , colSpan: null
    , rowSpan: null
    //
    , height: null
    , width: null
    , fixedSize: null
    , align: null
    , valign: null
    , balign: null
    //
    , face: null
    , pointSize: null
    //
    , bgColor: null
    , color: null
    //
    , bold: null
    , italic: null
    , underline: null
    , strike: null
    , head: {
      val: 'num'
      , face: null
      , pointSize: null
      , color: null
      //
      , bold: null
      , italic: null
      , underline: null
      , strike: null
    }
  }
  , name: {
    port: null
    , border: 1
    , sides: null
    , cellSpacing: null
    , cellPadding: null
    //
    , colSpan: null
    , rowSpan: null
    //
    , height: null
    , width: null
    , fixedSize: null
    , align: null
    , valign: null
    , balign: null
    //
    , face: null
    , pointSize: null
    //
    , bgColor: null
    , color: null
    //
    , bold: null
    , italic: null
    , underline: null
    , strike: null
    , head: {
      val: 'num'
      , face: null
      , pointSize: null
      , color: null
      //
      , bold: null
      , italic: null
      , underline: null
      , strike: null
    }
  }
  , count: {
    port: 'count'
    , border: 1
    , sides: null
    , cellSpacing: null
    , cellPadding: null
    //
    , colSpan: null
    , rowSpan: null
    //
    , height: null
    , width: null
    , fixedSize: null
    , align: null
    , valign: null
    , balign: null
    //
    , face: null
    , pointSize: null
    //
    , bgColor: null
    , color: null
    //
    , bold: null
    , italic: null
    , underline: null
    , strike: null
    , head: {
      val: 'count'
      , face: null
      , pointSize: null
      , color: null
      //
      , bold: null
      , italic: null
      , underline: null
      , strike: null
    }
  }
};
module.exports = TableLabel;
