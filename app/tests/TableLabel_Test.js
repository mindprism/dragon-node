'use strict';
// var requirejs = require('requirejs');
//
// requirejs.config({
//    baseUrl: '.',
//   paths:{
//     //src:'./app/src'
//     //src:'app/src'
//     s:'../src'
//   },
//    nodeRequire: require
// });

const test = require('unit.js');
delete Object.prototype.should;
const chai = require('chai');
const expect = chai.expect;   // eslint-disable-line no-unused-vars
const assert = chai.assert;   // eslint-disable-line no-unused-vars
const should = chai.should(); // eslint-disable-line no-unused-vars
const demand = require('unexpected');

//noinspection SillyAssignmentJS
test.and = test.and;
//noinspection SillyAssignmentJS
test.array = test.array;
//noinspection SillyAssignmentJS
test.bool = test.bool;
//noinspection SillyAssignmentJS
test.case = test.case;
//noinspection SillyAssignmentJS
test.date = test.date;
//noinspection SillyAssignmentJS
test.dump = test.dump;
//noinspection SillyAssignmentJS
test.error = test.error;
//noinspection SillyAssignmentJS
test.exception = test.exception;
//noinspection SillyAssignmentJS
test.fail = test.fail;
//noinspection SillyAssignmentJS
test.function = test.function;
//noinspection SillyAssignmentJS
test.given = test.given;
//noinspection SillyAssignmentJS
test.if = test.if;
//noinspection SillyAssignmentJS
test.number = test.number;
//noinspection SillyAssignmentJS
test.object = test.object;
//noinspection SillyAssignmentJS
test.regexp = test.regexp;
//noinspection SillyAssignmentJS
test.stats = test.stats;
//noinspection SillyAssignmentJS
test.string = test.string;
//noinspection SillyAssignmentJS
test.then = test.then;
//noinspection SillyAssignmentJS
test.undefined = test.undefined;
//noinspection SillyAssignmentJS
test.value = test.value;
//noinspection SillyAssignmentJS
test.wait = test.wait;
//noinspection SillyAssignmentJS
test.when = test.when;

test

  

function webstorm(){
  this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](expect); // !unuc - lint hack for webstorm
  this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](assert); // !unuc - lint hack for webstorm
  this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](should); // !unuc - lint hack for webstorm
  this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](demand); // !unuc - lint hack for webstorm
}

const TableLabel = require('../src/TableLabel');

describe('TableLabel_Test', function() {
  this["UNUSED CLASS\n"] && this["UNUSED CLASS\n"](webstorm); // !unuc - lint hack for webstorm
  let opts={};
  //noinspection MagicNumberJS
  let nnc = {
    num: {val: 1, hdr: 'num'}
    , name: {val: 'name'}
    , count: {val: 100, hdr: 'name'}
  };
  let tl=new TableLabel(null,nnc,opts);
  
  before(function() { // eslint-disable-line
    // runs before all tests in this block
    //expect.equal(1,1);
  });
  after(function() { // eslint-disable-line
    // runs after all tests in this block
  });
  beforeEach(function() {
    // runs before each test in this block
  });
  afterEach(function() {
    // runs after each test in this block
  });
  // test cases
  it('should work with demand', function() {
    demand(3,'to equal',3);
  });
  it('should work with test', function() {
    test.when("I want",function(){
      test.string('');
      //test.string(1);
    });
  });
  it('should cover webstorm', function() {
    assert.isUndefined(tl.webstorm());
    //demand(1,'to equal',2);
  });
  it('should exist', function() {
    TableLabel.should.be.a('function');
    //demand(1,'to equal',2);
  });
  it('should be constuctable', function() {
    tl.should.be.an('object');
  });
  it('should have options equal to default options', function() {
    assert.deepEqual(tl.options,TableLabel.defaultOptions);
  });
  it('should produce a string for _table()', function() {
    console.log(tl._table(''));
    tl._table('').should.be.a('string');
  });
  
});
