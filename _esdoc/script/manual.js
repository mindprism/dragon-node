
//  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.css">

window._={
  _name:'_'
  ,ls:{
    _name:'ls'
    ,supports:function(){
      return lscache.supported();
    }
    ,cut:function (key, data,deep){
      var o=this.get(key);
      if (typeof o!=='object') {
        throw new Error('stored data is not object');
      }
      if (o===null||o===undefined) {
        o={};
        return this.set(key,o);
      }
      $.reduce(o,data,deep);
      return this.set(key,o);
    }
    ,put:function (key, data,deep){
      if ( deep===undefined) {
        deep=true;
      }
      if (typeof data!=='object') {
        throw new Error('data is not object');
        return;
      }
      var o=this.get(key);
      if (typeof o!=='object'&&o!==undefined) {
        throw new Error('stored data is not object');
      }
      if (o===null||o===undefined) {
        o={};
      }
      var oo;
      if (deep) {
        oo=$.extend(true,o,data);
      }else{
        oo=$.extend(true,o,data);
      }
      return lscache.set(key,oo);
    }
    ,set:function (key, data, exp){
      return lscache.set(key,data,exp);
    }
    ,get:function (key,def){
      var r=lscache.get(key);
      if (r===null) {
        return def;
      }
      return r;
    }
    ,remove:function(key){
      return lscache.flushItem(key)
    }
    ,flush:function (){
      return lscache.flush();
    }
    ,flushExpired:function (){
      return lscache.flushExpired();
    }
    ,bucket:function (b){
      return lscache.setBucket(b);
    }
    ,resetBucket:function (){
      return lscache.resetBucket();
    }
    ,enableWarnings:function(enable){
      return lscache.enableWarnings(enable);
    }
  }
  ,utils:{
    name:'utils'
    ,getPageUrl:function(){
      var u=location.href.toString().split('/');
      return u[u.length-1];
    }
    ,clipPut:function(s){
      $('body').append('<div id="tempclip" />');
      $('#tempclip').attr('data-clipboard-text',s)
      .trigger( "click" );
      $('#tempclip').remove();
    }
    ,clipMsg:function(what,data){
      _.utils.clipPut(data);
      $.notify(what+' "'+data+'" copied to clipboard','success',{'class':'metro'});
    }
    ,sledMsg:function(fp){
      location.href=fp.sled;$.notify('Edited "'+fp.fullPath+'"','success',{'class':'metro'});
    }
    ,showPopupMenu:function(me,menu){
      me.contextMenu('popup',menu,{onClose:function(data,event){$(data.menu[0]).remove();}});
    }
    ,isPageItem:function(){
      return '.content>.header-notice>[data-ice=kind]'.q.length===1;
    }
    ,getPageItemType:function(){
      return '.content>.header-notice>[data-ice=kind]'.q.text();
    }
    ,getTip:function(o){
      var def={
        style:{
          classes:'qtip-dark qtip-rounded qtip-shadow'
        }
        ,show: {
          delay:500
        }
        ,content:{
        }
      };
      return $.extend(true,{},def,o);
    }
    ,getMenuTip:function(){
      return _.utils.getTip({
        content:{
          text:function(event,api){return 'Click for Menu';}
        }
      });
    }
    ,getKindHtmlFromType: function (k){
      return '<span class="kind kind-'+k+'">'+k.substr(0,1).toUpperCase()+'</span>';
    }
    ,getKindHtml: function (me){
        var fp=_.utils.filePathFromHr(me.attr('href'));var k=fp.type;
        return this.getKindHtmlFromType(k);
    }
    ,getInlineExtIconHtmlFromExt:function(ext,cls){
      return '<span class="'+cls+' _'+ext+'-i"/>';
    }
    ,getInlineExtIconHtml:function(me,cls){
      var fp=_.utils.filePathFromHr(me.attr('href'));
      return this.getInlineExtIconHtmlFromExt(fp.ext,cls);
    }
    ,readTextFile:function (file){
      var rawFile = new XMLHttpRequest();
      rawFile.open("GET", file, false);
      rawFile.onreadystatechange = function (){
        if(rawFile.readyState === 4){
          if(rawFile.status === 200 || rawFile.status == 0){
            var allText = rawFile.responseText;
            alert(allText);
          }
        }
      }
      rawFile.send(null);
    }
    ,writeTextFile2:function(content, filename){
      var dlg = false;
      with(document){
       ir=createElement('iframe');
       ir.id='ifr';
       ir.location='about.blank';
       ir.style.display='none';
       body.appendChild(ir);
        with(getElementById('ifr').contentWindow.document){
           open("text/plain", "replace");
           charset = "utf-8";
           write(content);
           close();
           document.charset = "utf-8";
           dlg = execCommand('SaveAs', false, filename+'.txt');
        }
        body.removeChild(ir);
      }
      return dlg;
    }
    ,downloadFile:function(strData, strFileName, strMimeType) {
        var D = document,
            A = arguments,
            a = D.createElement("a"),
            d = A[0],
            n = A[1],
            t = A[2] || "text/plain";
        //build download link:
        a.href = "data:" + strMimeType + "charset=utf-8," + escape(strData);
        if (window.MSBlobBuilder) { // IE10
            var bb = new MSBlobBuilder();
            bb.append(strData);
            return navigator.msSaveBlob(bb, strFileName);
        } /* end if(window.MSBlobBuilder) */
        if ('download' in a) { //FF20, CH19
            a.setAttribute("download", n);
            a.innerHTML = "downloading...";
            D.body.appendChild(a);
            setTimeout(function() {
                var e = D.createEvent("MouseEvents");
                e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                a.dispatchEvent(e);
                D.body.removeChild(a);
            }, 66);
            return true;
        }; /* end if('download' in a) */
        //do iframe dataURL download: (older W3)
        var f = D.createElement("iframe");
        D.body.appendChild(f);
        f.src = "data:" + (A[2] ? A[2] : "application/octet-stream") + (window.btoa ? ";base64" : "") + "," + (window.btoa ? window.btoa : escape)(strData);
        setTimeout(function() {
            D.body.removeChild(f);
        }, 333);
        return true;
    }
    ,writeTextFile:function(content, filename){
      return;
      var requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
      // requestFileSystem(type, size, successCallback, opt_errorCallback);
      //  type - Whether the file storage should be persistent. Possible values are window.TEMPORARY or window.PERSISTENT. Data stored using TEMPORARY can be removed at the browser's discretion (for example if more space is needed). PERSISTENT storage cannot be cleared unless explicitly authorized by the user or the app and requires the user to grant quota to your app. See requesting quota.
      //  size - Size (in bytes) the app will require for storage.
      //  successCallback - Callback that is invoked on successful request of a file system. Its argument is a FileSystem object.
      //  opt_errorCallback - Optional callback for handling errors or when the request to obtain the file system is denied. Its argument is a FileError object.
      //
      function onInitFs(fs) {
        console.log('Opened file system: ' + fs.name);
      }
      function errorHandler(e) {
        var msg = '';
        switch (e.code) {
          case FileError.QUOTA_EXCEEDED_ERR:
            msg = 'QUOTA_EXCEEDED_ERR';
            break;
          case FileError.NOT_FOUND_ERR:
            msg = 'NOT_FOUND_ERR';
            break;
          case FileError.SECURITY_ERR:
            msg = 'SECURITY_ERR';
            break;
          case FileError.INVALID_MODIFICATION_ERR:
            msg = 'INVALID_MODIFICATION_ERR';
            break;
          case FileError.INVALID_STATE_ERR:
            msg = 'INVALID_STATE_ERR';
            break;
          default:
            msg = 'Unknown Error';
            break;
        };
        console.log('Error: ' + msg);
      }
      //requestFileSystem(window.TEMPORARY, 5*1024*1024 /*5MB*/, onInitFs, errorHandler);
      window.webkitStorageInfo.requestQuota(PERSISTENT, 1024*1024, function(grantedBytes) {
        requestFileSystem(PERSISTENT, grantedBytes, onInitFs, errorHandler);
      }, function(e) {
        console.log('Error', e);
      });
    }
    ,filePathFromHr:function(url,peelHtml){
      // esdoc/XXX
      // filePathFromHr.type: class
      // filePathFromHr.sled: sled+fullPath
      // filePathFromHr.fileNameOnly: file
      // filePathFromHr.fileNameExt: file.ext
      // filePathFromHr.fullPath: C:\...src/...<file>
      // filePathFromHr.relPath: src/...dest/<file>
      // filePathFromHr.relPathOnly: src/...dest<nofile>
      var ro={};
      //
      var pfx='C:\\_\\captable\\bitbucket\\captable-rt\\';
      var hr=url;
      var c='~';
      if (hr.indexOf('#')!==-1) {
        c='#';
      }
      var hr=hr.split(c);
      var type=hr[0].split('/');
      type=type[0];
      ro.type=type;
      var path=hr[0].substr(type.length+1);
      if (peelHtml) {
        path=path.replace(/\.html$/,'');
      }
      ro.pathPart=path;
      ro.pathPartOnly=path.replace(/\/[^/]*$/,'');
      ro.fullPath=pfx+path.replace(/\//g,'\\');
      ro.fileNameExt=path.split('/');
      ro.fileNameExt=ro.fileNameExt[ro.fileNameExt.length-1];
      var ext=path.split('.');
      if (ext.length>2) {
        ext=ext[ext.length-2];
      }else{
        ext=ext[ext.length-1];
      }
      ro.ext=ext;
      //sled://vseditlc/C%3A%5C%21mark%5C_tool%5Ctw5_vpw%5Ctw5a.js/3000/13
      ro.sled='sled://vsedit/'+escape(ro.fullPath);

      var rro={};
      rro.ext=ro.ext;
      rro.fileNameExt=ro.fileNameExt;
      rro.fileNameOnly=ro.fileNameExt.replace(/\..*/,'');
      rro.fullPath=ro.fullPath;
      rro.relPath=ro.pathPart;
      rro.relPathOnly=ro.pathPartOnly;
      rro.sled=ro.sled;
      rro.type=ro.type;
      return rro;
    }
    ,addStylesheetAsynch:function(url,id,cb){
      var stylesheet = loadCSS( url,$('#jqueryjs').get(0),undefined,id);
      onloadCSS( stylesheet, cb);
    }
    ,addStylesheet:function(url,id){
      var style = document.createElement('link');
      style.rel = 'stylesheet';
      style.type = 'text/css';
      style.href = url;
      if (id) {
        style.id=id;
      }
      (document.head||document.documentElement).appendChild(style);
      } //-addStylesheet
    ,addScript:function(url,id,cb){
      var el = document.createElement('script');
      el.type = 'text/javascript';
      el.src = url;
      if (id) {
        el.id=id;
      }
      if (cb) {
        el.async = true;
        el.onload = cb;
      }
      (document.head||document.documentElement).appendChild(el);
      } //-addScript
  }
  ,qs:{
    _name:'qs'
    ,navUl:               "nav.navigation>div>ul"
    ,navDirPaths:         "nav.navigation>div>ul>li[data-ice=doc]>div[data-ice=dirPath]"
    ,navDocLis:           "nav.navigation>div>ul>li[data-ice=doc]"
    ,navDocLiNames:       "nav.navigation>div>ul>li[data-ice=doc]>span[data-ice=name]"
    ,navDocLinks:         "nav.navigation>div>ul>li[data-ice=doc]>span[data-ice=name]>span>a"
    ,navDocLinksKindClass:"nav.navigation>div>ul>li[data-ice=doc]>span.kind-class"
    ,referencesPageTableSummay:"table.summary"
    ,referencesPageTrs:"table.summary>tbody>tr[data-ice=target]"
    ,referencesPageLinks:"table.summary>tbody>tr[data-ice=target] span[data-ice=name]>span>a"
    ,sourcePageTableSummary:"table.files-summary"
    ,sourcePageTableTrs:"table.files-summary>tbody>tr[data-ice=file]"
    ,sourcePageFileLinks:"table.files-summary>tbody>tr[data-ice=file]>td[data-ice=filePath]>span>a"
    ,sourcePageIdentLinks:"table.files-summary>tbody>tr[data-ice=file]>td[data-ice=identifier]>span>a"
  }
  ,inits:{
    _name:'inits'
    ,loads:{
      _name:'loads'
      ,items:[
        ['jqueryjs','script/jquery-2.1.4.min.js']
        ,['clipboardjs','script/clipboard.min.js']  //https://zenorocha.github.io/clipboard.js
        ,['loadcssjs','script/loadCSS.js']
        ,['contextmenujs','script/contextMenu.min.js']   //https://github.com/s-yadav/contextMenu.js
        ,['onloadcssjs','script/onloadCSS.js']
        ,['jqueryqtipjs','script/jquery.qtip.min.js']
        ,['notifyjs','script/notify.min.js']            // https://notifyjs.com/
        ,['lscachejs','script/lscache.min.js']            // https://github.com/pamelafox/lscache
        ,['jqueryqtipcss','css/jquery.qtip.min.css']
        ,['styleqtipcss','css/style-qtip.css']
        ,['contextmenucss','css/contextMenu.css']        //http://ignitersworld.com/lab/contextMenu.html#
        ]
      ,at:0
      ,load:function(cb){
        //debugger;
        if (typeof cb==='function') {
          this.load.cb=cb;
        }
        if (this.at===this.items.length) {
          if (this.load.cb) {
            cb=this.load.cb;
            cb();
          }
          return;
        }
        var i=this.items[this.at];
        console.log('loading:'+i[0],i[1]);
        if (/\.js$/.test(i[1])) {
          _.utils.addScript(i[1],i[0],this.load.bind(this));
        }else if (/\.css$/.test(i[1])) {
          _.utils.addStylesheetAsynch(i[1],i[0],this.load.bind(this));
        }
        this.at++;
        //items.forEach()
      }
    }
    ,doNavDocLinksKindClassClips:function(){
      _.qs.navDocLinksKindClass.q.css('cursor','pointer');
      var qo=_.qs.navUl.q;
      qo.find('[data-ice=kind]').qtip(_.utils.getMenuTip());
      // Menu       TODO fix to use all kinds
      qo.on('mouseup','.kind-class',function(){
        var me=$(this);
        var hr=me.parent().find('[data-ice=name]>span>a');
        var ident=hr.text();
        var fp=_.utils.filePathFromHr(hr.attr('href'));
        var menu = [
          {name: 'Copy Identifier'
          ,fun: function () {_.utils.clipMsg('Identifier',ident);}}
          ,{name: 'Copy Filename'
          ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
          ,{name: 'Copy Filename Only'
          ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
          ,{name: 'Copy Fullpath'
          ,fun: function () {_.utils.clipMsg('FullPath',fp.fullPath);}}
          ,{name: 'Copy Relative Path'
          ,fun: function () {_.utils.clipMsg('Relative Path',fp.relPath);}}
          ,{name: 'Copy Relative Path Only'
          ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
          ,{name: 'Edit '+fp.fileNameExt
          ,fun: function () {_.utils.sledMsg(fp);}}
        ];
        _.utils.showPopupMenu(me,menu);
      });
    }//-doNavDocLinksKindClassClips
    ,doNavDocLiNamesClasses:function(){
      //TODO change to inject html
      var qo=_.qs.navDocLiNames.q;
      qo.each(function(){
        var me=$(this);
        var lnk=me.find('a');
        var fp=_.utils.filePathFromHr(lnk.attr('href'));
        me.addClass('_'+fp.ext+'-b');
      });
    }//-doNavDocLiNamesClasses
    ,doNavDocLinksKindClassTips:function(){
      var qo=_.qs.navDocLinks.q;
      qo.qtip(_.utils.getTip({
        content:{
          title:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'));
            return fp.type+' '+me.text()+' in '+fp.fileNameExt;
          }
          ,text:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'));
            return '<span style="white-space:nowrap;">'+fp.relPath+'</span>';
          }
        }}));
    }//-doNavDocLinksKindClassTips
    ,doFoldableNavSections:function(){
      function doCount(me){
        var li=me.parent();var ct=1;
        while (li.next().length!==0) {
          li=li.next();
          if (li.find('.nav-dir-path').length===1) {break;}
          ct+=1;
        }
        return ct;
      }
      function getList(me){
        var li=me.parent();var s='';
        s+='<li>'+li.find('[data-ice=name] a').text()+'</li>';
        while (li.next().length!==0) {
          li=li.next();
          if (li.find('.nav-dir-path').length===1) {break;}
          s+='<li>'+li.find('[data-ice=name] a').text()+'</li>';
        }
        s='<ul>'+s+'</ul>';
        return s;
      }
      function doFold(me,unfold){
        var ls_verb='put';var class_verb='addClass';var display_val='none';
        if (unfold) {ls_verb='cut';class_verb='removeClass';display_val='';}
        var oo={};oo[me.text()]=true;
        _.ls[ls_verb]('side-nav-folds',oo);
        me[class_verb]('folded');
        var li=me.parent();
        //
        li.find('span').css('display',display_val);
        while (li.next().length!==0) {
          li=li.next();
          if (li.find('.nav-dir-path').length===1) {break;}
          li.find('span').css('display',display_val);
        }
      }
      function doAll(unfold){
        _.qs.navDirPaths.q.each(function(){doFold($(this),unfold);});
      }
      // Fold on Click
      var qo=_.qs.navUl.q;
      qo.on('click','li[data-ice=doc]>div[data-ice=dirPath]',function(event){
        var me=$(this);var all=event.ctrlKey;
        if (all) {doAll(me.hasClass('folded'));}else{doFold(me,me.hasClass('folded'));}
      });
      // Tip Fold Headers
      var qo=_.qs.navDirPaths.q;
      qo.qtip(_.utils.getTip({
        content:{
          title:function(event,api){
            var me=$(event.currentTarget);
            return me.attr('ct')+' items in '+me.text();
          }
          ,text:function(event,api){
            var me=$(event.currentTarget);
            return 'Ctrl to fold/unfold all<br/>'+getList(me);
          }
        }
      }));
      // Set folds from localstor
      var folds=_.ls.get('side-nav-folds',{});
      qo.each(function(){
        var me=$(this);
        me.attr('ct',doCount(me));
        var txt=me.text();
        if (folds[txt]!==undefined) {doFold(me,false);}
      });
    }//-doFoldableNavSections
    ,doReferencesPage:function(){
      if (_.utils.getPageUrl()!='identifiers.html') {return;}
      // Count Rows
      var qo=_.qs.referencesPageLinks.q;
      qo.each(function(i){
        var me=$(this);
        var tgt=me.parents('tr').find('td');
        tgt.attr('data-row',i+1);
        me.attr('data-row',i+1);
      });
      // Links Tips
      qo=_.qs.referencesPageLinks.q;
      qo.qtip(_.utils.getTip({
        position:{
          my:'right center'
          ,at:'left center'
        }
        ,content:{
          title:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'));
            return fp.type+' '+me.text()+' in '+fp.fileNameExt;
          }
          ,text:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'));
            return '<span style="white-space:nowrap;">'+fp.relPath+'</span>';
          }
        }
      }));
      // Add kind to links
      qo.each(function(){
        var me=$(this);
        var fp=_.utils.filePathFromHr(me.attr('href'));
        me.addClass('_'+fp.ext+'-b');
        me.parents('td').prev().append(_.utils.getKindHtml(me));
      });
      // Kind Menu
      qo=_.qs.referencesPageTableSummay.q;
      qo.on('mouseup','.kind',function(event){
        var me=$(event.currentTarget);
        var hr=me.parent().next().find('[data-ice=name]>span>a');
        var ident=hr.text();
        var fp=_.utils.filePathFromHr(hr.attr('href'));
        var menu = [
         {name: 'Copy Identifier'
         ,fun: function () {_.utils.clipMsg('Identifier',ident);}}
         ,{name: 'Copy Filename'
         ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
         ,{name: 'Copy Filename Only'
         ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
         ,{name: 'Copy Fullpath'
         ,fun: function () {_.utils.clipMsg('Fullpath',fp.fullPath);}}
         ,{name: 'Copy Relative Path'
         ,fun: function () {_.utils.clipMsg('Relative Path',fp.relPath);}}
         ,{name: 'Copy Relative Path Only'
         ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
         ,{name: 'Edit '+fp.fileNameExt
         ,fun: function () {_.utils.sledMsg(fp);}
        }];
        _.utils.showPopupMenu(me,menu);
      });
      qo.find('.kind').qtip(_.utils.getMenuTip());
    }//-doReferencesPage
    ,doSourcePage:function(){
      if (_.utils.getPageUrl()!='source.html') {return;}
      var qo=_.qs.sourcePageTableTrs.q;
      /// TD counter
      qo.each(function(i){
        var me=$(this);me.find('td').attr('data-row',i+1);
      });
      /// File Tips
      qo=_.qs.sourcePageFileLinks.q;
      qo.qtip(_.utils.getTip({
        position:{
          my:'top left'
          ,at:'bottom left'
        }
        ,content:{
          title:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'),true);
            return fp.fileNameExt+" in "+fp.relPathOnly;
          }
          ,text:function(event,api){
            var me=$(event.currentTarget);
            var tr=me.parent().parent().parent();
            var td=tr.find('td.identifiers');
            var links=td.find('a');
            var ul_html='';
            links.each(function(){
              var me2=$(this);
              var fp=_.utils.filePathFromHr(me2.attr('href'));
              var s='';
              s+='<li>'+fp.type+' '+me2.text()+'</li>';
              ul_html+=s;
            });
            ul_html='<ul>'+ul_html+'</ul>';
            return ul_html;
          }
        }
      }));
      /// File Icons
      qo.each(function(){
        var me=$(this);
        me.parent().prepend(_.utils.getInlineExtIconHtml(me,'sourceFileMenu'));
      });
      /// File Icon Tips
      '.sourceFileMenu'.q.qtip(_.utils.getMenuTip());
      /// File Menus
      _.qs.sourcePageTableSummary.q.on('mouseup','.sourceFileMenu',function(event){
        var me=$(event.currentTarget);
        var hr=me.parent().find('a');
        var ident=hr.text();
        var peelHtml=true;
        var fp=_.utils.filePathFromHr(hr.attr('href'),peelHtml);
        var menu = [
          {name: 'Copy Filename'
          ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
          ,{name: 'Copy Filename Only'
          ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
          ,{name: 'Copy Fullpath'
          ,fun: function () {_.utils.clipMsg('Fullpath',fp.fullPath);}}
          ,{name: 'Copy Relative Path'
          ,fun: function () {_.utils.clipMsg('Relative Path',ident);}}
          ,{name: 'Copy Relative Path Only'
          ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
          ,{name: 'Edit '+fp.fileNameExt
          ,fun: function () {_.utils.sledMsg(fp);}}
        ];
        _.utils.showPopupMenu(me,menu);
      });
      /// Identifier Tips
      qo=_.qs.sourcePageIdentLinks.q;
      qo.qtip(_.utils.getTip({
        position:{
          my:'top left'
          ,at:'bottom left'
        }
        ,content:{
          title:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'),true);
            return fp.type+" "+me.text()+" in "+fp.fileNameExt;
          }
          ,text:function(event,api){
            var me=$(event.currentTarget);
            var fp=_.utils.filePathFromHr(me.attr('href'));
            return '<span style="white-space:nowrap;">'+fp.relPathOnly+'</span>';
          }
        }
      }));
      /// Identifier Icons
      qo.each(function(){
        var me=$(this);
        me.parent().prepend(_.utils.getInlineExtIconHtml(me,'sourceIdentMenu'));
      });
      /// Identifier Icons Tips
      '.sourceIdentMenu'.q.qtip(_.utils.getMenuTip());
      /// Identifier type Icons
      qo.each(function(){
        var me=$(this);
        me.parent().prepend(_.utils.getKindHtml(me));
      });
      // Kind Menu Tip
      '.kind'.q.qtip(_.utils.getMenuTip());
      // Kind Menu
      _.qs.sourcePageTableSummary.q.on('mouseup','.kind,.sourceIdentMenu',function(event){
        var me=$(event.currentTarget);
        var hr=me.parent().find('a');
        var ident=hr.text();
        var peelHtml=false;
        var fp=_.utils.filePathFromHr(hr.attr('href'),peelHtml);
        var menu = [
          {name: 'Copy Identifier'
          ,fun: function () {_.utils.clipMsg('Identifier',ident);}}
          ,{name: 'Copy Filename'
          ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
          ,{name: 'Copy Filename Only'
          ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
          ,{name: 'Copy Fullpath'
          ,fun: function () {_.utils.clipMsg('FullPath',fp.fullPath);}}
          ,{name: 'Copy Relative Path'
          ,fun: function () {_.utils.clipMsg('Relative Path',fp.relPath);}}
          ,{name: 'Copy Relative Path Only'
          ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
          ,{name: 'Edit '+fp.fileNameExt
          ,fun: function () {_.utils.sledMsg(fp);}}
        ];
        _.utils.showPopupMenu(me,menu);
      });
    }//-doSourcePage
    ,doPageTitle:function(){
      var i='.content>h1>span'.q.detach();
      var title='.content>h1'.q.text();
      if (_.utils.getPageUrl()==='index.html') {
        title='Home';
      }
      var type;
      if ('.content>.self-detail>h1'.q.length!==0) {
        type='.content>.header-notice>[data-ice=kind]'.q.text();
        //title=type+' '+'.content>.self-detail>h1'.q.text();
        title='.content>.self-detail>h1'.q.text();
      }
      '.content>h1'.q.append(i);
      '[data-ice=repoURL]'.q.after('<span class="pageTitle">'+title+'</span>');
      var hr='.content>.header-notice>[data-ice=source] a'.q;
      var fp=_.utils.filePathFromHr(hr.attr('href'),true);
      var qo='.pageTitle'.q;
      if (type) {
        qo.before(_.utils.getInlineExtIconHtmlFromExt(fp.ext,'pageTitleExt'));
        qo.before(_.utils.getKindHtmlFromType(type));
      }
      qo='header>.kind,header>.pageTitleExt'.q;
      var ident=title;
      qo.on('mouseup',function(event){
        var me=$(this);
        var menu = [
          {name: 'Copy Identifier'
          ,fun: function () {_.utils.clipMsg('Identifier',ident);}}
          ,{name: 'Copy Filename'
          ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
          ,{name: 'Copy Filename Only'
          ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
          ,{name: 'Copy Fullpath'
          ,fun: function () {_.utils.clipMsg('FullPath',fp.fullPath);}}
          ,{name: 'Copy Relative Path'
          ,fun: function () {_.utils.clipMsg('Relative Path',fp.relPath);}}
          ,{name: 'Copy Relative Path Only'
          ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
          ,{name: 'Edit '+fp.fileNameExt
          ,fun: function () {_.utils.sledMsg(fp);}}
        ];
        _.utils.showPopupMenu(me,menu);
      });
      qo='header>.pageTitle'.q;
      qo.on('mouseup',function(event){
        var me=$(this);
        var menu = [];
        '.content [data-ice]>h2'.q.each(function(){
          var me=$(this);
          var txt=me.text();
          me.attr('data-name',txt);
          var obj={};
          obj.name=txt;
          obj.fun=function(){
            console.log(me);
            $('html, body').animate({
                scrollTop: me.offset().top-100
            }, 1000);
          }
          menu.push(obj);
        });
        _.utils.showPopupMenu(me,menu);
      });

    }
    ,doItemPage:function(){
      if (!_.utils.isPageItem()) {return;}
      // count rows and toggle
      var qo='table.summary'.q;
      qo.each(function(){
        var me=$(this)
        me.find('tbody>tr').each(function(rownum){
          var row=$(this);
          row.find('>td').each(function(){
             var td=$(this);
             td.attr('data-row',rownum+1);
          });
        });
        me.find('thead>tr>td').attr('data-rows',me.find('tbody>tr').length);
        me.find('thead>tr>td:not(.toggle)').css('cursor','pointer').on('click',function(){
          var me=$(this);
          var tbody=me.parent().parent().next();
          tbody.toggle();
          //if (tbody.hasClass()) {}
        });
      });
      qo='.content [data-ice]>h2,.flat-list>h4'.q;
      qo.css('cursor','pointer').on('click',function(){
        var me=$(this);
        var next=me.next();
        next.toggle();
        while(next.next().length) {
          next=next.next();
          next.toggle();
        }
      });
      function breakUps(ice){
        var qo;
        // set the count
        qo=('[data-ice='+ice+']>div').q;
        var ct=qo.find('span').length;
        var h4=qo=('[data-ice='+ice+']>h4').q;
        h4.attr('ct',ct);
        //
        qo=('[data-ice='+ice+']>div').q;
        qo.contents()
        .filter(function(){ return this.nodeType != 1; })
        .wrap("<span class='orphan'/>");
        //
        qo=('[data-ice='+ice+']>div>.orphan').q;
        qo.remove();
        //
        qo=('[data-ice='+ice+']>div>span').q;
        qo.wrap('<div/>');
        var al;
        var prev;
        qo=('[data-ice='+ice+']>div>div>span').q;
        //console.log(qo);
        qo.each(function(){
          var me=$(this);
          var fl=me.find('a').text().substr(0,1);
          if (al!==fl) {
            if (al!==undefined) {
              me.parent().css('flex','100%').css('box-shadow','2px 2px #000');
              if (prev) {
                prev.css('flex-grow','0');
              }
            }else{
              me.parent().css('flex','100%').css('box-shadow','2px 2px #000');
            }
          }
          prev=me;
          al=fl;
        });
        //
        qo=('[data-ice='+ice+']>div>div>span>a').q;
        qo.each(function(){
          var me=$(this);
          me.before(_.utils.getKindHtml(me));
          me.before(_.utils.getInlineExtIconHtml(me,'subclassItem'));
        });
        //
        qo=('[data-ice='+ice+']>div>div>span').q;
        qo.on('mouseup','.kind,.subclassItem',function(event){
          var me=$(event.currentTarget);
          var hr=me.parent().find('a');
          var ident=hr.text();
          var peelHtml=false;
          var fp=_.utils.filePathFromHr(hr.attr('href'),peelHtml);
          var menu = [
            {name: 'Copy Identifier'
            ,fun: function () {_.utils.clipMsg('Identifier',ident);}}
            ,{name: 'Copy Filename'
            ,fun: function () {_.utils.clipMsg('Filename',fp.fileNameExt);}}
            ,{name: 'Copy Filename Only'
            ,fun: function () {_.utils.clipMsg('Filename Only',fp.fileNameOnly);}}
            ,{name: 'Copy Fullpath'
            ,fun: function () {_.utils.clipMsg('FullPath',fp.fullPath);}}
            ,{name: 'Copy Relative Path'
            ,fun: function () {_.utils.clipMsg('Relative Path',fp.relPath);}}
            ,{name: 'Copy Relative Path Only'
            ,fun: function () {_.utils.clipMsg('Relative Path Only',fp.relPathOnly);}}
            ,{name: 'Edit '+fp.fileNameExt
            ,fun: function () {_.utils.sledMsg(fp);}}
          ];
          _.utils.showPopupMenu(me,menu);
        });
      }
      breakUps('directSubclass');
      breakUps('indirectSubclass');
      // h2 counts
      qo='.content>[data-ice]>h2'.q;
      qo.each(function(){
        var me=$(this);
        var table=me.next('table');
        if (table.length===0) {
          return;
        }
        var rc=table.find('thead>tr>td').attr('data-rows');
        me.attr('data-private-count',0);
        me.attr('data-public-count',rc);
        table=table.next('table');
        if (table.length) {
          rc=rc=table.find('thead>tr>td').attr('data-rows');
          me.attr('data-private-count',rc);
        }
        var content=me.attr('data-public-count')+'/'+me.attr('data-private-count');
        me.attr('data-content',content);
        me.prepend('');
      });
      //detail counts
      qo='.content>[data-ice]>h2+div'.q;
      qo.each(function(){
        var me=$(this);
        var prev=me.prev();
        var h2=me.prev();
        var ct=1;
        var next=prev.next('div');
        next.attr('data-row',ct);
        next.find('h3').attr('data-row',ct);
        while (next.next('div').length) {
          next=next.next('div');
          ct++;
          next.attr('data-row',ct);
          next.find('h3').attr('data-row',ct);
        }
        h2.attr('data-content',ct);
      });


    }
    ,doClipStd:function(){
      new Clipboard('[data-clipboard-text]');
    }
  }
  ,protos:function(){
    Object.defineProperty(String.prototype,"q",{
      get:function(){
        var s=this.toString();
        return $(s);
      }
    });
    $.reduce = function reduce(obj1, obj2,deep) {
      if (typeof deep===undefined) {
        deep=true;
      }
      for (var k in obj2) {
        if (obj1.hasOwnProperty(k) && obj2.hasOwnProperty(k)) {
          if (typeof obj1[k] == "object" && typeof obj2[k] == "object" && deep) {
             reduce(obj1[k], obj2[k],deep);
          }
          else delete obj1[k];
        }
      }
    }
    //String.prototype.q=
  }
  ,init2:function(){
    console.log('init2');
    this.protos();
    _.ls.bucket('esdoc-');
    this.inits.doPageTitle();
    this.inits.doNavDocLinksKindClassClips();
    this.inits.doNavDocLiNamesClasses();
    this.inits.doClipStd();
    this.inits.doNavDocLinksKindClassTips();
    this.inits.doFoldableNavSections();
    this.inits.doReferencesPage();
    this.inits.doSourcePage();
    this.inits.doItemPage();
  }
  ,init:function(){
    this.inits.loads.load(this.init2.bind(this));
  }
};
!(function(){
  //https://code.jquery.com/jquery-2.2.3.min.js
  _.init();
  // (function(d, script) {
  //     script = d.createElement('script');
  //     script.type = 'text/javascript';
  //     script.async = true;
  //     script.onload = function(){
  //         // remote script has loaded
  //       (function(d, script) {
  //           script = d.createElement('script');
  //           script.type = 'text/javascript';
  //           script.async = true;
  //           script.onload = function(){
  //               // remote script has loaded
  //             $(function(){
  //               console.log('document loaded');
  //               window._.init();
  //             });
  //           };
  //           script.src = 'script/clipboard.min.js';
  //           d.getElementsByTagName('head')[0].appendChild(script);
  //       }(document));
  //     };
  //     script.src = 'https://code.jquery.com/jquery-2.2.3.min.js';
  //     d.getElementsByTagName('head')[0].appendChild(script);
  // }(document));

  var matched = location.pathname.match(/([^/]*)\.html$/);
  if (!matched) return;

  var currentName = matched[1];
  var cssClass = '.navigation [data-toc-name="' + currentName + '"]';
  var styleText = cssClass + ' .manual-toc { display: block; }\n';
  styleText += cssClass + ' .manual-toc-title { background-color: #039BE5; }\n';
  styleText += cssClass + ' .manual-toc-title a { color: white; }\n';
  var style = document.createElement('style');
  style.textContent = styleText;
  document.querySelector('head').appendChild(style);
})();

